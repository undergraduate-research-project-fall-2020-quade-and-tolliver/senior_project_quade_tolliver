console.log("Running client_register_request.js!  by luther and jeffrey");
console.log("goood");
function send_register_client(client_register_dictionary){
    console.log("client register's toPython() is called!");
    var url = 'http://178.128.155.104/backend/client_register';
    var json_object = JSON.stringify(client_register_dictionary);
    console.log (json_object);
    var jsonObject = JSON.stringify(client_register_dictionary);

    //making a bridge to the web server
    var request = new XMLHttpRequest();
    //initializing request to the web server
    request.open('POST',  url, true);
    //Setting the header of the API request
    request.setRequestHeader("Content-type", "application/json");

    //do something with the data being processed
    request.onreadystatechange = function()
    {
       console.log("request.onload is called!");

       // Good response
      if (request.status >= 200 && request.status < 400 && this.readyState == 4){
        console.log("Request is sent!");
        console.log("Response: " + request.response);
        console.log(request.statusText);
        alert("Register of client " + client_register_dictionary['username'] + " successful!");
        var favoriteusername = client_register_dictionary['username'];
        sessionStorage.setItem("favoriteusername", favoriteusername);

        var favoritepassword = client_register_dictionary['password'];
        sessionStorage.setItem("favoritepassword", favoritepassword);

        var favoritephone = client_register_dictionary['phone'];
        sessionStorage.setItem("favoritephone", favoritephone);

        var favoriteemail = client_register_dictionary['email'];
        sessionStorage.setItem("favoriteemail", favoriteemail);


        window.location.href="/client_restaurantbuilder_ifTheyDidnotMakeOne.html";
        //load_page("loginPage.html");
      }

      else if (request.status == 403)
      {
        showFailure()
      }      

      else if (request.status >= 400) {
        //Error handling
        const errorMessage = document.createElement('error');
        errorMessage.textContent = "Connection Error!";
        alert(request.status + " FAILED: Register of client " + client_register_dictionary['username'] + " failed! (" + request.response + ")");
        console.log(request.status);
      }
    }

        //request get sent out!
      request.send(json_object);


}
// Function that will show an alert to the user if he/she enters a incorrect username or password
function showFailure()
{
  alert("Luther Tolliver show failure!")
}