console.log("Running client_login_request.js!  by luther and jeffrey");
console.log("goood");
function send_login_client(client_login_dictionary){
    console.log("client login's toPython() is called!");
    var url = 'http://178.128.155.104/backend/client_login';
    var json_object = JSON.stringify(client_login_dictionary);
    console.log (json_object);
    var jsonObject = JSON.stringify(client_login_dictionary);

    //making a bridge to the web server
    var request = new XMLHttpRequest();
    //initializing request to the web server
    request.open('POST',  url, true);
    //Setting the header of the API request
    request.setRequestHeader("Content-type", "application/json");

    //do something with the data being processed
    request.onreadystatechange = function()
    {
       console.log("request.onload is called!");

       // Good response
      if (request.status >= 200 && request.status < 400 && this.readyState == 4){
        console.log("Request is sent!");
        console.log("Response: " + request.response);
        console.log(request.statusText);
        alert("Login of client " + client_login_dictionary['username'] + " successful!");
        //load_page("loginPage.html");
        var usernameInsert = client_login_dictionary['username'];
        var usernameNewDictionary = {"username" : usernameInsert};

        send_login_client_toRestaurant_db_check(usernameNewDictionary);
      }

      else if (request.status >= 400) {
        //Error handling
        const errorMessage = document.createElement('error');
        errorMessage.textContent = "Connection Error!";
        alert(request.status + " FAILED: Login of client " + client_login_dictionary['username'] + " failed! (" + request.response + ")");
        console.log(request.status);
      }
    }

        //request get sent out!
      request.send(json_object);


}



function send_login_client_toRestaurant_db_check(client_info_dictionary){
  console.log("Second function in client login.")
  console.log(client_info_dictionary)
  var url = 'http://178.128.155.104/backend/does_client_register_restaurant_exist_login';
  var json_object_2 = JSON.stringify(client_info_dictionary);
  console.log (json_object_2);

  //making a bridge to the web server
  var request = new XMLHttpRequest();
  //initializing request to the web server
  request.open('POST',  url, true);
  //Setting the header of the API request
  request.setRequestHeader("Content-type", "application/json");

  //do something with the data being processed
  request.onreadystatechange = function()
  {
     console.log("request.onload is called!");

     // Good response
    if (request.status >= 200 && request.status < 400 && this.readyState == 4){
      console.log("Request is sent!");
      console.log("Response: " + request.response);
      console.log(request.statusText);
      alert("Client " + client_info_dictionary['username'] + " has a restaurant successful!");
      //load_page("loginPage.html");
      var usernameInsert1 = client_info_dictionary['username'];
      var usernameNewNewDictionary = {"username" : usernameInsert1};
      //load_page("loginPage.html");
      send_login_client_toRestaurant_items_db_check(usernameNewNewDictionary);
    }

    else if (request.status >= 400) {
      //Error handling
      const errorMessage = document.createElement('error');
      errorMessage.textContent = "Connection Error!";
      alert(request.status + " Client " + client_info_dictionary['username'] + " doesn't have a restaurant! (" + request.response + ")");
      //console.log(request.response);
      orderDict = JSON.parse(request.response)
      console.log(orderDict)
      console.log(orderDict[0].username)
      console.log(orderDict[0].password)
      console.log(orderDict[0].email)
      console.log(orderDict[0].phone)
      var testOrderDictionary = orderDict[0].username
      console.log(testOrderDictionary)
      var favoriteusername = orderDict[0].username;
      sessionStorage.setItem("favoriteusername", favoriteusername);

      var favoritepassword = orderDict[0].password;
      sessionStorage.setItem("favoritepassword", favoritepassword);

      var favoritephone = orderDict[0].phone;
      sessionStorage.setItem("favoritephone", favoritephone);

      var favoriteemail = orderDict[0].email;
      sessionStorage.setItem("favoriteemail", favoriteemail);
      console.log("Print Out Restaurant Info Musts: Username, Password, Phone, email")
      console.log(favoriteusername)
      console.log(favoritepassword)
      console.log(favoritephone)
      console.log(favoriteemail)

      window.location.href="/client_restaurantbuilder_ifTheyDidnotMakeOne.html";
    }
  }

      //request get sent out!
    request.send(json_object_2);
}



function send_login_client_toRestaurant_items_db_check(client_items_info_dictionary){
  console.log("Third function in client login.")
  console.log(client_items_info_dictionary)
  var url = 'http://178.128.155.104/backend/does_client_register_restaurant_items_exist_login';
  var json_object_3 = JSON.stringify(client_items_info_dictionary);
  console.log (json_object_3);

  //making a bridge to the web server
  var request = new XMLHttpRequest();
  //initializing request to the web server
  request.open('POST',  url, true);
  //Setting the header of the API request
  request.setRequestHeader("Content-type", "application/json");

  //do something with the data being processed
  request.onreadystatechange = function()
  {
     console.log("request.onload is called!");

     // Good response
    if (request.status >= 200 && request.status < 400 && this.readyState == 4){
      console.log("Request is sent!");
      console.log("Response: " + request.response);
      console.log(request.statusText);
      alert("Client " + client_items_info_dictionary['username'] + " has restaurant items, successful!");
      // AFTER THIS SEND CLIENT TO THEIR OWN INDIVIDUAL PAGE
      console.log("DID ALL 3 successful login")
      console.log(client_items_info_dictionary['username'])

      // send the user to their personal page

      var favoriteusername = client_items_info_dictionary['username'];
      sessionStorage.setItem("favoriteusername", favoriteusername);

      window.location.href="/clientTableItems.html";

      //var usernameInsert = client_info_dictionary['username'];
      //var usernameNewNewDictionary = {"username" : usernameInsert};
      //load_page("loginPage.html");
      //send_login_client_toRestaurant_items_db_check(usernameNewNewDictionary);
    }

    else if (request.status >= 400) {
      //Error handling
      const errorMessage = document.createElement('error');
      errorMessage.textContent = "Connection Error!";
      alert(request.status + " Client " + client_items_info_dictionary['username'] + " doesn't have a restaurant item! (" + request.response + ")");
      console.log(request.response);

      var registered_username = client_items_info_dictionary['username'];
      sessionStorage.setItem("registered_username", registered_username);

      window.location.href="/addItemsForFirstTime.html";
      // send the user to the iteM Maker page for client with a session.setItem equal to their username.
    }
  }

      //request get sent out!
    request.send(json_object_3);


}