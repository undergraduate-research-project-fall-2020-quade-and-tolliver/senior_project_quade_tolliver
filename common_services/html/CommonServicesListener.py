# Necessary imports
import http.server
from http.server import BaseHTTPRequestHandler
import json
import cs_database_utils


class CommonServicesListener(BaseHTTPRequestHandler):

    # POST Methods
    def do_POST(self):

        # get the path of the request
        path = self.path.split("/")[-1]

        # CUSTOMER LOGIN REQUEST
        if path == "customer_login":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the Username from the dictionary
            print('Username:', dictionary['username'])
            does_username_match = cs_database_utils.customer_login(dictionary['username'], dictionary['password'])

            # This print statement should either be "True" or "False"
            print("Does the Username and Password exist: " + str(does_username_match))

            # If does_username_match == True
            if does_username_match:

                user_details = cs_database_utils.get_names_from_username_for_customer(dictionary['username'])

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': does_username_match, 'Username': dictionary['username']}
                print(response_dict)
                res = json.dumps(response_dict)

                # Whichever way you choose, you need to have a string that you can convert to bytes.
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

            # If doesUsernameMatch == False
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Invalid email or password. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        # REGISTER CUSTOMER REQUEST
        elif path == "customer_register":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the First Name, Last Name, and Email from the dictionary
            print('First name:', dictionary['first_name'])
            print('Last name:', dictionary['last_name'])
            print('Email:', dictionary['email'])

            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_username_exist = cs_database_utils.customer_registration(dictionary['first_name'],
                                                          dictionary['last_name'],
                                                          dictionary['username'],
                                                          dictionary['password'],
                                                          dictionary['address'],
                                                          dictionary['city'],
                                                          dictionary['state'],
                                                          dictionary['phone'],
                                                          dictionary['email'])

            # This print statement should either be "True" or "False"
            print("Does the Username exist: " + str(does_username_exist))

            # If does_username_exist == False
            if not does_username_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'First name': dictionary['first_name'],
                                 'Last name': dictionary['last_name'],
                                 'Email': dictionary['email']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Email or Password already exist. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)


     # REGISTER CLIENT REQUEST
        elif path == "client_register":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the First Name, Last Name, and Email from the dictionary
            print('First name:', dictionary['first_name'])
            print('Last name:', dictionary['last_name'])
            print('Email:', dictionary['email'])

            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_username_exist = cs_database_utils.client_registration(dictionary['first_name'],
                                                          dictionary['last_name'],
                                                          dictionary['username'],
                                                          dictionary['password'],
                                                          dictionary['address'],
                                                          dictionary['city'],
                                                          dictionary['state'],
                                                          dictionary['phone'],
                                                          dictionary['email'])


            # This print statement should either be "True" or "False"
            print("Does the Username exist: " + str(does_username_exist))

            # If does_username_exist == False
            if not does_username_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'First name': dictionary['first_name'],
                                 'Last name': dictionary['last_name'],
                                 'Email': dictionary['email']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Email or Password already exist. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)
            
     # CLIENT LOGIN REQUEST
        elif path == "client_login":

             # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the Email from the dictionary
            print('Username:', dictionary['username'])
            does_username_match = cs_database_utils.client_login(dictionary['username'], dictionary['password'])

            # This print statement should either be "True" or "False"
            print("Does the Username and Password exist: " + str(does_username_match))

            # If does_username_match == True
            if does_username_match:

                user_details = cs_database_utils.get_names_from_username_for_client(dictionary['username'])

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': does_username_match, 'Username': dictionary['username']}
                print(response_dict)
                res = json.dumps(response_dict)

                # Whichever way you choose, you need to have a string that you can convert to bytes.
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

            # If doesUsernameMatch == False
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Invalid email or password. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        elif path == "client_register_restaurant":
            
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
            
            # Prints the Business Name, Username, and Password from the dictionary
            print('Business Name:', dictionary['business_name'])
            print('Username:', dictionary['username'])
            print('Password:', dictionary['password'])
            
            
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_exist = cs_database_utils.client_register_restaurant(dictionary['business_name'],
                                                          dictionary['username'],
                                                          dictionary['password'],
                                                          dictionary['business_motto'],
                                                          dictionary['business_description'],
                                                          dictionary['business_email'],
                                                          dictionary['business_phone'],
                                                          dictionary['business_street'],
                                                          dictionary['business_city'],
                                                          dictionary['business_state'],
                                                          dictionary['sunday'],
                                                          dictionary['monday'],
                                                          dictionary['tuesday'],
                                                          dictionary['wednesday'],
                                                          dictionary['thursday'],
                                                          dictionary['friday'],
                                                          dictionary['saturday'])
            
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_exist))
            
            # If does_username_exist == False
            if not does_restaurant_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'Username': dictionary['username'],
                                 'Password': dictionary['password'],
                                 'Restaurant': dictionary['business_name']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Found an error with your registration input for the restaurant. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        elif path == "client_register_restaurant_items":
            
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
            print(dictionary)



            
            # Prints the Business Name, Username, and Password from the dictionary
            print(dictionary['itemName'])
            print(dictionary['username'])
            
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_item_exist = cs_database_utils.client_register_restaurant_items(dictionary['username'],
                                                          dictionary['itemName'],
                                                          dictionary['itemPrice'],
                                                          dictionary['itemDescription'])
            
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_item_exist))
            
            
            # If does_username_exist == False
            if not does_restaurant_item_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'Username': dictionary['username'],
                                 'Item Name': dictionary['itemName']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "You tried to either add an already item or your restaurant hasn't been made yet. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        elif path == "client_register_table_items":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                    
            # Prints the Business Name, Username, and Password from the dictionary
            print(' Client Username:', dictionary['username'])
                    
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_items_table_exist = cs_database_utils.getClientItems(dictionary['username'])
                    
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_items_table_exist))
                    
            serviceOrderList = does_restaurant_items_table_exist
            orderDict = serviceOrderList
            #updateStatus = utils.updateVehicleStatus(licensePlate)

            # If everything went well, send a 200
            self.send_response(200)

            # Very important, or your response will be slow!
            # Signify that you are done sending Headers
            self.end_headers()

            # Dump fleetDict as a JSON object and encode 'utf-8'
            # Whichever way you choose, you need to have a string that you can convert to bytes.
            res = json.dumps(orderDict)
            bytesStr = res.encode('utf-8')

            self.end_headers()
            self.wfile.write(bytesStr)

        elif path == "customer_search_bar_restaurant_names":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # Gets the service orders from the DB
            #licensePlate = utils.getLicensePlate()
            #print(licensePlate)
            serviceOrderList = cs_database_utils.getCustomer_ClientBusinessNames()
            orderDict = serviceOrderList
            #updateStatus = utils.updateVehicleStatus(licensePlate)

            # If everything went well, send a 200
            self.send_response(200)

            # Very important, or your response will be slow!
            # Signify that you are done sending Headers
            self.end_headers()

            # Dump fleetDict as a JSON object and encode 'utf-8'
            # Whichever way you choose, you need to have a string that you can convert to bytes.
            res = json.dumps(orderDict)
            bytesStr = res.encode('utf-8')

            self.end_headers()
            self.wfile.write(bytesStr)

        elif path == "does_client_register_restaurant_exist_login":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                    
            # Prints the Business Name, Username, and Password from the dictionary
            print('Username:', dictionary['username'])
            
            does_username_business_exist = cs_database_utils.make_sure_client_has_restaurant_info(dictionary['username'])
            if does_username_business_exist:
                # If everything went well, send a 200
                self.send_response(200)
                
                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': does_username_business_exist, 'Username': dictionary['username']}
                print(response_dict)
                res = json.dumps(response_dict)

                # Whichever way you choose, you need to have a string that you can convert to bytes.
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)
                
                
            else:
                serviceOrderList = cs_database_utils.make_sure_client_has_restaurant_info_Fail_get_dict(dictionary['username'])
                orderDict = serviceOrderList
                self.send_response(404)
                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # Dump fleetDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(orderDict)
                bytesStr = res.encode('utf-8')

                self.end_headers()
                self.wfile.write(bytesStr)

        elif path == "does_client_register_restaurant_items_exist_login":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                    
            # Prints the Business Name, Username, and Password from the dictionary
            print('Username:', dictionary['username'])
            
            does_username_business_items_exist = cs_database_utils.make_sure_client_has_restaurant_items_info(dictionary['username'])
            
            if does_username_business_items_exist:
                # If everything went well, send a 200
                self.send_response(200)
                
                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': does_username_business_items_exist, 'Username': dictionary['username']}
                print(response_dict)
                res = json.dumps(response_dict)

                # Whichever way you choose, you need to have a string that you can convert to bytes.
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)
            else:
                self.send_response(404)
                self.end_headers()
                res = "You have no items in your restaurant, so we will be send you to a page to make some."
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        elif path == "client_edit_page_for_business":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                            
            # Prints the Business Name, Username, and Password from the dictionary
            print(' Restaurant client owner:', dictionary['username'])
                            
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_client_restaurant_table_exist = cs_database_utils.getClientBusinessInformationAndRegister(dictionary['username'])
                            
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_client_restaurant_table_exist))
                            
            serviceOrderList = does_client_restaurant_table_exist
            orderDict = serviceOrderList
            #updateStatus = utils.updateVehicleStatus(licensePlate)

            # If everything went well, send a 200
            self.send_response(200)

            # Very important, or your response will be slow!
            # Signify that you are done sending Headers
            self.end_headers()

            # Dump fleetDict as a JSON object and encode 'utf-8'
            # Whichever way you choose, you need to have a string that you can convert to bytes.
            res = json.dumps(orderDict)
            bytesStr = res.encode('utf-8')

            self.end_headers()
            self.wfile.write(bytesStr)

        elif path == "client_change_register_information":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the First Name, Last Name, and Email from the dictionary
            print('First name:', dictionary['first_name'])
            print('Last name:', dictionary['last_name'])
            print('Email:', dictionary['email'])

            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_new_information_already_exist = cs_database_utils.clientChangeRegistrationInformation(dictionary['first_name'],
                                                          dictionary['last_name'],
                                                          dictionary['username'],
                                                          dictionary['password'],
                                                          dictionary['address'],
                                                          dictionary['city'],
                                                          dictionary['state'],
                                                          dictionary['phone'],
                                                          dictionary['email'])


            # This print statement should either be "True" or "False"
            print("Does the Username exist: " + str(does_new_information_already_exist))

            # If does_username_exist == False
            if not does_new_information_already_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'First name': dictionary['first_name'],
                                 'Last name': dictionary['last_name'],
                                 'Email': dictionary['email']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Email or Phone already exist. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)


            
        elif path == "client_change_restaurant_information":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the First Name, Last Name, and Email from the dictionary
            print('Username:', dictionary['username'])
            print('Business Name:', dictionary['business_name'])
            print('business_motto', dictionary['business_motto'])

            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_new_information_already_exist = cs_database_utils.clientChangeRestaurantInformation(dictionary['username'],
                                                          dictionary['business_name'],
                                                          dictionary['business_motto'],
                                                          dictionary['business_description'],
                                                          dictionary['business_email'],
                                                          dictionary['business_phone'],
                                                          dictionary['business_street'],
                                                          dictionary['business_city'],
                                                          dictionary['business_state'],
                                                          dictionary['sunday'],
                                                          dictionary['monday'],
                                                          dictionary['tuesday'],
                                                          dictionary['wednesday'],
                                                          dictionary['thursday'],
                                                          dictionary['friday'],
                                                          dictionary['saturday'])



            # This print statement should either be "True" or "False"
            print("Does the Username exist: " + str(does_new_information_already_exist))

            # If does_username_exist == False
            if not does_new_information_already_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'First name': dictionary['first_name'],
                                 'Last name': dictionary['last_name'],
                                 'Email': dictionary['email']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Email or Phone already exist. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)




        elif path == "customer_want_client_table_items":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                    
            # Prints the Business Name, Username, and Password from the dictionary
            print(' Restaurant Name:', dictionary['business_name'])
                    
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_items_table_exist = cs_database_utils.getClientItemsForCustomer(dictionary['business_name'])
                    
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_items_table_exist))
                    
            serviceOrderList = does_restaurant_items_table_exist
            orderDict = serviceOrderList
            #updateStatus = utils.updateVehicleStatus(licensePlate)

            # If everything went well, send a 200
            self.send_response(200)

            # Very important, or your response will be slow!
            # Signify that you are done sending Headers
            self.end_headers()

            # Dump fleetDict as a JSON object and encode 'utf-8'
            # Whichever way you choose, you need to have a string that you can convert to bytes.
            res = json.dumps(orderDict)
            bytesStr = res.encode('utf-8')

            self.end_headers()
            self.wfile.write(bytesStr)


        elif path == "client_want_client_items_to_change_and_or_delete":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                            
            # Prints the Business Name, Username, and Password from the dictionary
            print(' Restaurant Name:', dictionary['username'])
                            
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_items_table_exist = cs_database_utils.getClientItemsForEditAndDelete(dictionary['username'])
                            
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_items_table_exist))
                            
            serviceOrderList = does_restaurant_items_table_exist
            orderDict = serviceOrderList
            #updateStatus = utils.updateVehicleStatus(licensePlate)

            # If everything went well, send a 200
            self.send_response(200)

            # Very important, or your response will be slow!
            # Signify that you are done sending Headers
            self.end_headers()

            # Dump fleetDict as a JSON object and encode 'utf-8'
            # Whichever way you choose, you need to have a string that you can convert to bytes.
            res = json.dumps(orderDict)
            bytesStr = res.encode('utf-8')

            self.end_headers()
            self.wfile.write(bytesStr)

            
        elif path == "client_want_client_items_to_edit":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
            print(dictionary)



                    
            # Prints the Business Name, Username, and Password from the dictionary
            print(dictionary['itemName'])
            print(dictionary['username'])
                    
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_item_exist = cs_database_utils.client_edit_restaurant_items(dictionary['original_item_name'],dictionary['username'],dictionary['itemName'],dictionary['itemPrice'],dictionary['itemDescription'])
                    
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_item_exist))
                    
                    
            # If does_username_exist == False
            if not does_restaurant_item_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                'Username': dictionary['username'],
                                'Item Name': dictionary['itemName']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

                # If doesUsernameExist == True
                # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "You tried to either add an already item or your restaurant hasn't been made yet. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)



        elif path == "client_want_client_items_to_delete":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
            print(dictionary)



                    
            # Prints the Business Name, Username, and Password from the dictionary
            print(dictionary['itemName'])
            print(dictionary['username'])
                    
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_restaurant_item_exist = cs_database_utils.client_delete_restaurant_items(dictionary['username'],dictionary['itemName'])
                    
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_restaurant_item_exist))
                    
                    
            # If does_username_exist == False
            if not does_restaurant_item_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                'Username': dictionary['username'],
                                'Item Name': dictionary['itemName']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

                # If doesUsernameExist == True
                # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "You tried to either add an already item or your restaurant hasn't been made yet. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        elif path == "customer_register_items_bought_from_restaurant":
                # You can get all headers via self.headers
                # or identify which header you want to get
                print('Headers:"', self.headers, '""')
                print('Content-Type:', self.headers['content-type'])

                # This is how you read the body of the request

                # First grab he length of the body and read that many characters
                length = int(self.headers['content-length'])
                body = self.rfile.read(length)

                # at this point you can print the whole body
                # if you want (good for debugging)
                # at this point body is in bytes
                print("Body (String): ", body.decode())

                # How to convert the body from a string to a dictionary
                # use 'loads' to convert from byte/string to a dictionary!
                dictionary = json.loads(body)
                print(dictionary)



                        
                # Prints the Business Name, Username, and Password from the dictionary
                print(dictionary['itemName'])
                print(dictionary['customer_username'])
                
                # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
                does_restaurant_item_exist = cs_database_utils.customer_buy_client_restaurant_items(dictionary['date'], 
                                                                    dictionary['customer_username'],
                                                                    dictionary['restaurant_name'],
                                                                    dictionary['client_username'],
                                                                    dictionary['itemName'],
                                                                    dictionary['price'],
                                                                    dictionary['price_for_one'],
                                                                    dictionary['amount_of_sales'],
                                                                    dictionary['order_status'],
                                                                    dictionary['notes'])
                                                                    
                                                                    
                                                                    
                # This print statement should either be "True" or "False"
                print("Does the Restaurant exist: " + str(does_restaurant_item_exist))
                
                
                # If does_username_exist == False
                if not does_restaurant_item_exist:
                    # If everything went well, send a 200
                    self.send_response(200)
                    # Very important, or your response will be slow!
                    # Signify that you are done sending Headers
                    self.end_headers()
                    # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                    # This response_dict will be sent back to the Front End and will show the response in the console
                    response_dict = {'Success': True,
                                    'Username': dictionary['customer_username'],
                                    'itemName': dictionary['itemName'],
                                    'Restaurant': dictionary['restaurant_name']}
                    print(response_dict)
                    # Dump vehicleDict as a JSON object and encode 'utf-8'
                    # Whichever way you choose, you need to have a string that you can convert to bytes.
                    res = json.dumps(response_dict)
                    bytesStr = res.encode('utf-8')
                    self.wfile.write(bytesStr)

                # If doesUsernameExist == True
                # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
                else:
                    self.send_response(404)
                    self.end_headers()
                    res = "Found an error with your itemName for the restaurant. Code 404"
                    bytesStr = res.encode('utf-8')
                    self.wfile.write(bytesStr)
                    
        elif path == "client_side_retrieveClientOrdersByUsernameOfClient":
                # You can get all headers via self.headers
                # or identify which header you want to get
                print('Headers:"', self.headers, '""')
                print('Content-Type:', self.headers['content-type'])
                # This is how you read the body of the request
                # First grab he length of the body and read that many characters
                length = int(self.headers['content-length'])
                body = self.rfile.read(length)
                # at this point you can print the whole body
                # if you want (good for debugging)
                # at this point body is in bytes
                print("Body (String): ", body.decode())
                # How to convert the body from a string to a dictionary
                # use 'loads' to convert from byte/string to a dictionary!
                dictionary = json.loads(body)
                # Prints the Business Name, Username, and Password from the dictionary
                print(' Restaurant client owner:', dictionary['username'])
                # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
                does_client_restaurant_order_exist = cs_database_utils.getClientBusinessOrders(dictionary['username'])
                # This print statement should either be "True" or "False"
                print("Does the Restaurant exist: " + str(does_client_restaurant_order_exist))
                serviceOrderList = does_client_restaurant_order_exist
                orderDict = serviceOrderList
                #updateStatus = utils.updateVehicleStatus(licensePlate)
                # If everything went well, send a 200
                self.send_response(200)
                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()
                # Dump fleetDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(orderDict)
                bytesStr = res.encode('utf-8')
                self.end_headers()
                self.wfile.write(bytesStr)

        elif path == "client_side_updateClientOrdersByUsernameOfClient":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the First Name, Last Name, and Email from the dictionary
            print('First name:', dictionary['client_username'])
            print('Last name:', dictionary['date'])
            print('Email:', dictionary['customer_username'])

            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_new_information_already_exist = cs_database_utils.updateClientBusinessOrders(dictionary['amount_of_sales'],
                                                          dictionary['client_username'],
                                                          dictionary['customer_username'],
                                                          dictionary['date'],
                                                          dictionary['itemName'],
                                                          dictionary['notes'],
                                                          dictionary['order_status'],
                                                          dictionary['price'],
                                                          dictionary['price_for_one'],
                                                          dictionary['restaurant_name'])


            # This print statement should either be "True" or "False"
            print("Does the Username exist: " + str(does_new_information_already_exist))

            print("Does the Username exist: " + str(does_new_information_already_exist) + str(dictionary['amount_of_sales']) + str(dictionary['client_username']) + str(dictionary['customer_username'])+str(dictionary['date']) + str(dictionary['itemName']) + str(dictionary['notes']) + str(dictionary['order_status'])+ str(dictionary['price']) + str(dictionary['price_for_one']) + str(dictionary['restaurant_name']))

            # If does_username_exist == False
            if not does_new_information_already_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'First name': dictionary['client_username'],
                                 'Last name': dictionary['date'],
                                 'Email': dictionary['customer_username']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

        elif path == "customer_edit_page_for_business":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)
                            
            # Prints the Business Name, Username, and Password from the dictionary
            print(' Restaurant client owner:', dictionary['username'])
                            
            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_client_restaurant_table_exist = cs_database_utils.getCustomerInformationAndRegister(dictionary['username'])
                            
            # This print statement should either be "True" or "False"
            print("Does the Restaurant exist: " + str(does_client_restaurant_table_exist))
                            
            serviceOrderList = does_client_restaurant_table_exist
            orderDict = serviceOrderList
            #updateStatus = utils.updateVehicleStatus(licensePlate)

            # If everything went well, send a 200
            self.send_response(200)

            # Very important, or your response will be slow!
            # Signify that you are done sending Headers
            self.end_headers()

            # Dump fleetDict as a JSON object and encode 'utf-8'
            # Whichever way you choose, you need to have a string that you can convert to bytes.
            res = json.dumps(orderDict)
            bytesStr = res.encode('utf-8')

            self.end_headers()
            self.wfile.write(bytesStr)

        elif path == "customer_change_register_information":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '""')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request

            # First grab he length of the body and read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print("Body (String): ", body.decode())

            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            dictionary = json.loads(body)

            # Prints the First Name, Last Name, and Email from the dictionary
            print('First name:', dictionary['first_name'])
            print('Last name:', dictionary['last_name'])
            print('Email:', dictionary['email'])

            # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
            does_new_information_already_exist = cs_database_utils.customerChangeRegistrationInformation(dictionary['first_name'],
                                                          dictionary['last_name'],
                                                          dictionary['username'],
                                                          dictionary['password'],
                                                          dictionary['address'],
                                                          dictionary['city'],
                                                          dictionary['state'],
                                                          dictionary['phone'],
                                                          dictionary['email'])


            # This print statement should either be "True" or "False"
            print("Does the Username exist: " + str(does_new_information_already_exist))

            # If does_username_exist == False
            if not does_new_information_already_exist:

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # If you don't want to format your JSON, make a dictionary, and convert it to a string with 'json.dumps'
                # This response_dict will be sent back to the Front End and will show the response in the console
                response_dict = {'Success': True,
                                 'First name': dictionary['first_name'],
                                 'Last name': dictionary['last_name'],
                                 'Email': dictionary['email']}
                print(response_dict)

                # Dump vehicleDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(response_dict)
                bytesStr = res.encode('utf-8')

                self.wfile.write(bytesStr)

            # If doesUsernameExist == True
            # Send a status code 403 Forbidden response back to the console and explain why the user is getting a 403 Forbidden
            else:

                self.send_response(404)
                self.end_headers()
                res = "Email or Phone already exist. Code 404"
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

        elif path == "customer_side_retrieveClientOrdersByUsernameOfClient":
                # You can get all headers via self.headers
                # or identify which header you want to get
                print('Headers:"', self.headers, '""')
                print('Content-Type:', self.headers['content-type'])
                # This is how you read the body of the request
                # First grab he length of the body and read that many characters
                length = int(self.headers['content-length'])
                body = self.rfile.read(length)
                # at this point you can print the whole body
                # if you want (good for debugging)
                # at this point body is in bytes
                print("Body (String): ", body.decode())
                # How to convert the body from a string to a dictionary
                # use 'loads' to convert from byte/string to a dictionary!
                dictionary = json.loads(body)
                # Prints the Business Name, Username, and Password from the dictionary
                print(' Restaurant client owner:', dictionary['customer_username'])
                # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
                does_client_restaurant_order_exist = cs_database_utils.getCustomerBusinessOrders(dictionary['customer_username'])
                # This print statement should either be "True" or "False"
                print("Does the Restaurant exist: " + str(does_client_restaurant_order_exist))
                serviceOrderList = does_client_restaurant_order_exist
                orderDict = serviceOrderList
                #updateStatus = utils.updateVehicleStatus(licensePlate)
                # If everything went well, send a 200
                self.send_response(200)
                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()
                # Dump fleetDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(orderDict)
                bytesStr = res.encode('utf-8')
                self.end_headers()
                self.wfile.write(bytesStr)

        elif path == "customer_want_client_restaurant_info":
                # You can get all headers via self.headers
                # or identify which header you want to get
                print('Headers:"', self.headers, '""')
                print('Content-Type:', self.headers['content-type'])

                # This is how you read the body of the request

                # First grab he length of the body and read that many characters
                length = int(self.headers['content-length'])
                body = self.rfile.read(length)

                # at this point you can print the whole body
                # if you want (good for debugging)
                # at this point body is in bytes
                print("Body (String): ", body.decode())

                # How to convert the body from a string to a dictionary
                # use 'loads' to convert from byte/string to a dictionary!
                dictionary = json.loads(body)
                        
                # Prints the Business Name, Username, and Password from the dictionary
                print(' Restaurant Name:', dictionary['business_name'])
                        
                # Sends the First Name, Last Name, Email, and Password into the DB to be checked and verified
                does_restaurant_items_table_exist = cs_database_utils.getClientBusinessForCustomer(dictionary['business_name'])
                        
                # This print statement should either be "True" or "False"
                print("Does the Restaurant exist: " + str(does_restaurant_items_table_exist))
                        
                serviceOrderList = does_restaurant_items_table_exist
                orderDict = serviceOrderList
                #updateStatus = utils.updateVehicleStatus(licensePlate)

                # If everything went well, send a 200
                self.send_response(200)

                # Very important, or your response will be slow!
                # Signify that you are done sending Headers
                self.end_headers()

                # Dump fleetDict as a JSON object and encode 'utf-8'
                # Whichever way you choose, you need to have a string that you can convert to bytes.
                res = json.dumps(orderDict)
                bytesStr = res.encode('utf-8')

                self.end_headers()
                self.wfile.write(bytesStr)


    
    def do_OPTIONS(self):

        self.send_response(200)
        self.send_header('Allow', 'GET, POST, OPTIONS')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Headers', 'X-Request, X-Requested-With')
        #self.send_header('Content-Length', '0')
        self.send_header('Access-Control-Allow-Headers', 'Content-Type')
        self.end_headers()


def main():

    # Define the port your sever will run on:
    port = 4120

    # Create an http server using the class and port you defined
    httpServer = http.server.HTTPServer(('', port), CommonServicesListener)
    print("Running on port", port)

    # instructions on how to run without blocking other commands from being executed in your terminal!
    httpServer.serve_forever()


if __name__ == "__main__":
    main()