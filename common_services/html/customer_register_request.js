console.log("Running registerPageHandling.js");

function send_register_demand(register_dictionary) {
    //API key for connection
    console.log("register's toPython() is called!");
    var url = 'http://178.128.155.104/backend/customer_register';
    var json_object = JSON.stringify(register_dictionary);
    console.log (json_object);

    //making a bridge to the web server
    var request = new XMLHttpRequest();
    //initializing request to the web server
    request.open('POST',  url, true);
    //Setting the header of the API request
    request.setRequestHeader("Content-type", "application/json");

    //do something with the data being processed
    request.onreadystatechange = function()
    {
       console.log("request.onload is called!");

       // Good response
      if (request.status >= 200 && request.status < 400 && this.readyState == 4){
        console.log("Request is sent!");
        console.log("Response: " + request.response);
        console.log(request.statusText);
        alert("Registration of user " + register_dictionary['username'] + " successful!");
        var customer_username_dictionary_for_buying = {"customer_username":register_dictionary['username']};
        sessionStorage.setItem("customer_username_dictionary_for_buying", JSON.stringify(customer_username_dictionary_for_buying));
        window.location.href="/customer_mainpage_after_login_or_register.html";
        //load_page("http://178.128.155.104/customer_mainpage_after_login_or_register.html");
      }

      else if (request.status >= 400) {
        //Error handling
        const errorMessage = document.createElement('error');
        errorMessage.textContent = "Connection Error!";
        alert(request.status + " FAILED: Registration of user " + register_dictionary['username'] + " failed! (" + request.response + ")");
        console.log(request.status);
      }
    }

        //request get sent out!
      request.send(json_object);
}