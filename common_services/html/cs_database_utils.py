# Utilities file that will have many functions to handle what is needed from the DB

# Necessary imports
from pymongo import MongoClient

# Sets up the connection between our Python file and MongoDB
# Gains access to the DB and the collection that we need to access
uri = "mongodb://localhost:27017/"
client = MongoClient(uri)

# user_login(param1, param2) will return either "True" or "False" based on the parameters
# The user then will get a response back whether or not the login was successful or not
# If the email and password are in the DB then return "True" -> Else return "False"
def customer_login(username, password):

    db = client.demand
    collection = db.customer_register_data

    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"username": username, "password": password})

    # If the cursor is in the DB, return True
    if cursor is not None:

        return True

    # Else the cursor isn't in the DB, return False
    else:

        return False

# user_registration(param1, param2, param3, param4) will return either "True" or "False" based on the parameters
# If the email and password are in the DB then return "True"
# Else insert the new user into the DB and then return "False"
def customer_registration(first_name, last_name, username, password, address, city, state, phone, email):

    db = client.demand
    collection = db.customer_register_data

    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"email": email, "username": username})

    cursor_1 = collection.find_one({"username": username})

    cursor_2 = collection.find_one({"email": email})

    cursor_3 = collection.find_one({"phone": phone})

    if cursor_1 is not None:

        return True

    if cursor_2 is not None:

        return True

    if cursor_3 is not None:

        return True

    # If the cursor is in the DB, return True
    if cursor is not None:

        return True

    # Else insert the user into the DB, return False
    else:

        collection.insert_one({"first_name": first_name, "last_name": last_name, "username": username, "password": password, "address": address, "city": city, "state": state, "phone": phone,  "email": email})
        return False



def get_names_from_username_for_customer(username):

    db = client.demand
    collection = db.customer_register_data

    # Cursor will hold the value of the query and convert the id
    print(username)
    cursor = collection.find_one({"username": username})
    cursor['_id'] = str(cursor['_id'])
    print(cursor)
    # Return the query back to the API
    return cursor

"""
CLIENT SIDE TO DATABASE
"""


# user_login(param1, param2) will return either "True" or "False" based on the parameters
# The user then will get a response back whether or not the login was successful or not
# If the email and password are in the DB then return "True" -> Else return "False"
def client_login(username, password):

    db = client.supply
    collection = db.client_register_data

    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"username": username, "password": password})

    # If the cursor is in the DB, return True
    if cursor is not None:

        return True

    # Else the cursor isn't in the DB, return False
    else:

        return False

# user_registration(param1, param2, param3, param4) will return either "True" or "False" based on the parameters
# If the email and password are in the DB then return "True"
# Else insert the new user into the DB and then return "False"
def client_registration(first_name, last_name, username, password, address, city, state, phone, email):

    db = client.supply
    collection = db.client_register_data

    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"email": email, "username": username})

    cursor_4 = collection.find_one({"phone": phone, "username": username})

    cursor_1 = collection.find_one({"username": username})

    cursor_2 = collection.find_one({"email": email})

    cursor_3 = collection.find_one({"phone": phone})

    if cursor_1 is not None:

        return True

    if cursor_2 is not None:

        return True

    if cursor_3 is not None:

        return True

    # If the cursor is in the DB, return True
    if cursor is not None:

        return True

    if cursor_4 is not None:

        return True

    # Else insert the user into the DB, return False
    else:

        collection.insert_one({"first_name": first_name, "last_name": last_name, "username": username, "password": password, "address": address, "city": city, "state": state, "phone": phone,  "email": email})
        return False



def get_names_from_username_for_client(username):

    db = client.supply
    collection = db.client_register_data

    # Cursor will hold the value of the query and convert the id
    print(username)
    cursor = collection.find_one({"username": username})
    cursor['_id'] = str(cursor['_id'])
    print(cursor)
    # Return the query back to the API
    return cursor

# If the email and password are in the DB then return "True"
# Else insert the new user into the DB and then return "False"

#FINISHED WITH THE  client_register_restaurant
#NOW WORKING ON     client_register_restaurant_items 

def client_register_restaurant(business_name, username, password, business_motto, business_description, business_email, business_phone, business_street, business_city, business_state, sunday, monday, tuesday, wednesday, thursday, friday, saturday):

    db = client.supply
    collection = db.client_register_restaurant_data
    collection1 = db.client_register_data

    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"business_name": business_name, "username": username, "business_email": business_email, "business_phone" : business_phone })


    cursor_1 = collection.find_one({"business_name": business_name})
    
    cursor_12 = collection.find_one({"business_email": business_email})

    cursor_13 = collection.find_one({"business_phone": business_phone})
    
    cursor_14 = collection.find_one({"username": username})
    

    cursor_2 = collection1.find_one({"email": business_email, "username": username})

    cursor_3 = collection1.find_one({"phone": business_phone, "username": username})

    cursor_4 = collection1.find_one({"password": password, "username": username})
    
    cursor_5 = collection1.find_one({"phone": business_phone, "username": username, "email": business_email, "password": password })
    

    if cursor_1 is not None:

        return True
    
    if cursor_12 is not None:

        return True
    
    if cursor_13 is not None:

        return True
    
    if cursor_14 is not None:

        return True
    
    if cursor_2 is None:

        return True

    if cursor_3 is None:

        return True
    
    if cursor_4 is None:

        return True

    if cursor_5 is None:

        return True
    
    
    

    # If the cursor is in the DB, return True
    if (cursor is not None):

        return True

    # Else insert the user into the DB, return False
    else:

        collection.insert_one({"business_name" : business_name, "username" : username, "password" : password, "business_motto" : business_motto, "business_description" : business_description, "business_email" : business_email, "business_phone" : business_phone, "business_street" : business_street, "business_city" : business_city, "business_state" : business_state, "sunday" : sunday, "monday" : monday, "tuesday" : tuesday, "wednesday" : wednesday, "thursday" : thursday , "friday" : friday, "saturday" : saturday})
        return False

def client_register_restaurant_items(username, itemName, itemPrice, itemDescription):
    
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    
    # cursor will hold the value of the query collection.find_one()
    #cursor2 = collection2.find_one({"username": username, "itemName": itemName, "itemPrice" : itemPrice, "itemDescription" : itemDescription})
    
    cursor3 = collection2.find_one({"itemName": itemName, "username": username})
    
    cursor4 = collection.find_one({"username": username})
    
    if cursor4 is None:

        return True
    
    if cursor3 is not None:

        return True
    
    else:
        
        collection2.insert_one({ "username" : username, "itemName": itemName, "itemPrice" : itemPrice, "itemDescription" : itemDescription})
        
        return False

def getClientItems(username):
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    
    cursor4 = collection.find_one({"username": username})
    cursor3 = collection2.find_one({"username": username})
    
    if cursor4 is None:

        return True
        
    if cursor3 is None:

        return True

    clientItems = []

    for doc in collection2.find({}):
        if doc['username'] == username:
            dict = {
            'itemName': doc['itemName'],
            'itemPrice': doc['itemPrice'],
            'itemDescription': doc['itemDescription']
            }
            clientItems.append(dict)
        

    return clientItems

def getCustomer_ClientBusinessNames():
    db = client.supply
    collection = db.client_register_restaurant_data

    clientRestaurantNames = []

    for doc in collection.find({}):
        dict = {
            'business_name': doc['business_name']
            
        }
        clientRestaurantNames.append(dict)
        

    return clientRestaurantNames

def make_sure_client_has_restaurant_info(username):
    db = client.supply
    collection = db.client_register_restaurant_data
    
    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"username": username})

    # If the cursor is in the DB, return True
    if cursor is not None:

        return True
    else:
        return False
        

def make_sure_client_has_restaurant_info_Fail_get_dict(username):
    db = client.supply
    collection = db.client_register_data

    clientRegisterInfo = []

    for doc in collection.find({}):
        if doc['username'] == username:
            dict = {
            'username': doc['username'],
            'email': doc['email'],
            'phone': doc['phone'],
            'password': doc['password']
            }
            clientRegisterInfo.append(dict)
        

    return clientRegisterInfo

def make_sure_client_has_restaurant_items_info(username):
    db = client.supply
    collection = db.client_register_restaurant_data_items
    
    # cursor will hold the value of the query collection.find_one()
    cursor = collection.find_one({"username": username})

    # If the cursor is in the DB, return True
    if cursor is not None:

        return True
    else:
        return False

def getClientBusinessInformationAndRegister(username):
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    collection3 = db.client_register_data
    
    cursor4 = collection.find_one({"username": username})
    cursor3 = collection2.find_one({"username": username})
    
    if cursor4 is None:

        return True
        
    if cursor3 is None:

        return True

    clientItems = []

    for doc in collection.find({}):
        if doc['username'] == username:
            thisdict = {
            "business_name" : doc['business_name'],
            "username" : doc['username'], 
            "password" : doc['password'], 
            "business_motto" : doc['business_motto'],
            "business_description" : doc['business_description'],
            "business_email" : doc['business_email'],
            "business_phone" : doc['business_phone'], 
            "business_street" : doc['business_street'], 
            "business_city" : doc['business_city'], 
            "business_state" : doc['business_state'], 
            "sunday" : doc['sunday'], 
            "monday" : doc['monday'], 
            "tuesday" : doc['tuesday'], 
            "wednesday" : doc['wednesday'],
            "thursday" : doc['thursday'] , 
            "friday" : doc['friday'], 
            "saturday" : doc['saturday']
            }
            clientItems.append(thisdict)
            
    for doc in collection3.find({}):
        if doc['username'] == username:
            dict = {
            'first_name': doc['first_name'], 
            'last_name': doc['last_name'], 
            'username': doc['username'], 
            'password': doc['password'],
            'address': doc['address'],
            'city': doc['city'],
            'state': doc['state'], 
            'phone': doc['phone'],  
            'email': doc['email']
            }
            (dict.update(thisdict))
            clientItems.append(dict)

    return clientItems

def clientChangeRegistrationInformation(first_name, last_name, username, password, address, city, state, phone, email):

    db = client.supply
    collection = db.client_register_data

    clientItems = []
    
    for doc in collection.find({}):
        if doc['username'] == username:
            dict = {
            'password': doc['password']
            }
            
            if doc['email']==email:
                collection.update_one({"username": username},{"$set":{"email": email}})
            else:
                cursor1 = collection.find_one({"email": email})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"email": doc['email'] }})
                else:
                    collection.update_one({"username": username},{"$set":{"email": email}})
                    
            if doc['phone']==phone:
                collection.update_one({"username": username},{"$set":{"phone": phone}})
            else:
                cursor1 = collection.find_one({"phone": phone})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"phone": doc['phone']}})
                else:
                    collection.update_one({"username": username},{"$set":{"phone": phone}})
                    
            
                    
          
            collection.update_one({"username": username},{"$set":{"password": password}})
            collection.update_one({"username": username},{"$set":{"state": state}})
            collection.update_one({"username": username},{"$set":{"city": city}})
            collection.update_one({"username": username},{"$set":{"address": address}})
            collection.update_one({"username": username},{"$set":{"first_name": first_name}})
            collection.update_one({"username": username},{"$set":{"last_name": last_name}})
            
            #collection.update({"username": username, "password": password})
    return False




def clientChangeRestaurantInformation(username, business_name, business_motto, business_description, business_email, business_phone, business_street, business_city, business_state, sunday, monday, tuesday, wednesday, thursday, friday, saturday):

    db = client.supply
    collection = db.client_register_restaurant_data


    clientItems = []
    
    for doc in collection.find({}):
        if doc['username'] == username:
            dict = {
            'business_name': doc['business_name']
            }
            
            if doc['business_name']==business_name:
                collection.update_one({"username": username},{"$set":{"business_name": business_name}})
            else:
                cursor1 = collection.find_one({"business_name": business_name})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"business_name": doc['business_name'] }})
                else:
                    collection.update_one({"username": username},{"$set":{"business_name": business_name}})
                   
            
            if doc['business_email']==business_email:
                collection.update_one({"username": username},{"$set":{"business_email": business_email}})
            else:
                cursor1 = collection.find_one({"business_email": business_email})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"business_email": doc['business_email'] }})
                else:
                    collection.update_one({"username": username},{"$set":{"business_email": business_email}})
            
        
                    
            if doc['business_phone']== business_phone:
                collection.update_one({"username": username},{"$set":{"business_phone": business_phone}})
            else:
                cursor1 = collection.find_one({"business_phone": business_phone})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"business_phone": doc['business_phone']}})
                else:
                    collection.update_one({"username": username},{"$set":{"business_phone": business_phone}})
            
            
            
            
          #  clientItems.append(dict)
    # return clientItems
            collection.update_one({"username": username},{"$set":{"business_motto": business_motto}})
            collection.update_one({"username": username},{"$set":{"business_description": business_description}})
            collection.update_one({"username": username},{"$set":{"business_street": business_street}})
            collection.update_one({"username": username},{"$set":{"business_city": business_city}})
            collection.update_one({"username": username},{"$set":{"business_state": business_state}})
            collection.update_one({"username": username},{"$set":{"sunday": sunday}})
            collection.update_one({"username": username},{"$set":{"monday": monday}})
            collection.update_one({"username": username},{"$set":{"tuesday": tuesday}})
            collection.update_one({"username": username},{"$set":{"wednesday": wednesday}})
            collection.update_one({"username": username},{"$set":{"thursday": thursday}})
            collection.update_one({"username": username},{"$set":{"friday": friday}})
            collection.update_one({"username": username},{"$set":{"saturday": saturday}})
            
            
            #collection.update({"username": username, "password": password})
    return False




def getClientItemsForCustomer(business_name):
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    
    cursor4 = collection.find_one({"business_name": business_name})
    #cursor3 = collection2.find_one({"username": username})
    
    if cursor4 is None:

        return True
        
    #if cursor3 is None:

        #return True

    clientItems = []

    for doc in collection.find({}):
        if doc['business_name'] == business_name:
            dict = {
            'business_name': doc['business_name']
            }
            #clientItems.append(dict)
            foundUsername = doc['username']
            for doc2 in collection2.find({}):
                if doc2['username'] == foundUsername:
                    dict = {
                    'itemName': doc2['itemName'],
                    'itemPrice': doc2['itemPrice'],
                    'itemDescription': doc2['itemDescription']
                    # 'itemStatus': doc2['itemStatus']

                    }
                    clientItems.append(dict)


    return clientItems

def getClientItemsForEditAndDelete(username):
    db = client.supply
    collection = db.client_register_data
    collection2 = db.client_register_restaurant_data_items
    
    cursor4 = collection.find_one({"username": username})
    
    
    if cursor4 is None:

        return True
        
    #if cursor3 is None:

        #return True

    clientItems = []

    for doc2 in collection2.find({}):
        if doc2['username'] == username:
            dict = {
            'itemName': doc2['itemName'],
            'itemPrice': doc2['itemPrice'],
            'itemDescription': doc2['itemDescription']
                
            }
            clientItems.append(dict)


    return clientItems


def client_edit_restaurant_items(original_item_name,username, itemName, itemPrice, itemDescription):
    
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    
    # cursor will hold the value of the query collection.find_one()
    #cursor2 = collection2.find_one({"username": username, "itemName": itemName, "itemPrice" : itemPrice, "itemDescription" : itemDescription})
    
    cursor3 = collection2.find_one({"itemName": original_item_name, "username": username})
    
    cursor4 = collection.find_one({"username": username})
    
    if cursor4 is None:

        return True
    
    if cursor3 is None:

        return True

    else:
        
        collection2.update_one(cursor3,{"$set":{"itemName": itemName}})
        collection2.update_one(cursor3,{"$set":{"itemPrice": itemPrice}})
        collection2.update_one(cursor3,{"$set":{"itemDescription": itemDescription}})
        
        return False    


def client_delete_restaurant_items(username, itemName):
    
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    
    # cursor will hold the value of the query collection.find_one()
    #cursor2 = collection2.find_one({"username": username, "itemName": itemName, "itemPrice" : itemPrice, "itemDescription" : itemDescription})
    
    cursor3 = collection2.find_one({"itemName": itemName, "username": username})
    
    cursor4 = collection.find_one({"username": username})
    
    if cursor4 is None:

        return True
    
    if cursor3 is None:

        return True

    else:
        
        collection2.delete_one(cursor3)
        
        
        
        return False 

def customer_buy_client_restaurant_items(date, customer_username, restaurant_name, client_username, itemName, price, price_for_one, amount_of_sales, order_status, notes):
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_restaurant_buyers_receipt
    cursor4 = collection2.find_one({"restaurant_name": restaurant_name})
    
    print("Hello")
    
    for doc in collection.find({}):
        if doc['business_name']==restaurant_name:
            client_username1 = doc['username']
            collection2.insert_one({ "date": date ,"customer_username" : customer_username, "restaurant_name": restaurant_name ,"client_username" : client_username1,"itemName": itemName, "price" : price, "price_for_one" : price_for_one,"amount_of_sales": amount_of_sales, "order_status" : order_status, "notes" : notes})
            collection2.update_one(cursor4,{"$set":{"client_username": client_username1}})
            
            return False 
            

def getClientBusinessOrders(username):
    db = client.supply
    collection = db.client_restaurant_buyers_receipt
    
    cursor4 = collection.find_one({"client_username": username})
    
    
    if cursor4 is None:

        return True
        
    

    clientItems = []

    for doc in collection.find({}):
        if doc['client_username'] == username:
            thisdict = {
            "amount_of_sales" : doc['amount_of_sales'],
            "client_username" : doc['client_username'], 
            "customer_username" : doc['customer_username'], 
            "date" : doc['date'],
            "itemName" : doc['itemName'],
            "notes" : doc['notes'],
            "order_status" : doc['order_status'], 
            "price" : doc['price'], 
            "price_for_one" : doc['price_for_one'], 
            "restaurant_name" : doc['restaurant_name']
            }
            clientItems.append(thisdict)
            

    return clientItems

def updateClientBusinessOrders(amount_of_sales,client_username,customer_username,date,itemName,notes,order_status,price,price_for_one,restaurant_name):
    db = client.supply
    collection = db.client_restaurant_buyers_receipt
    
    cursor4 = collection.find_one({"restaurant_name": restaurant_name,"client_username": client_username, "customer_username": customer_username, "date": date, "itemName" : itemName})
    
    
    collection.update_one(cursor4,{"$set":{"order_status": order_status}})
    
    return False

def getCustomerInformationAndRegister(username):
    db = client.demand
    collection = db.customer_register_data
    
    
    cursor4 = collection.find_one({"username": username})
    
    if cursor4 is None:

        return True
    clientItems = []
    
    for doc in collection.find({}):
        if doc['username'] == username:
            thisdict = {'first_name':doc['first_name'], 
            "last_name": doc['last_name'], 
            "username": doc['username'], 
            "password": doc['password'], 
            "address": doc['address'], 
            "city": doc['city'], 
            "state": doc['state'], 
            "phone": doc['phone'],  
            "email": doc['email']
                
            }
            clientItems.append(thisdict)
    return clientItems

def customerChangeRegistrationInformation(first_name, last_name, username, password, address, city, state, phone, email):

    db = client.demand
    collection = db.customer_register_data

    clientItems = []
    
    for doc in collection.find({}):
        if doc['username'] == username:
            dict = {
            'password': doc['password']
            }
            
            if doc['email']==email:
                collection.update_one({"username": username},{"$set":{"email": email}})
            else:
                cursor1 = collection.find_one({"email": email})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"email": doc['email'] }})
                else:
                    collection.update_one({"username": username},{"$set":{"email": email}})
                    
            if doc['phone']==phone:
                collection.update_one({"username": username},{"$set":{"phone": phone}})
            else:
                cursor1 = collection.find_one({"phone": phone})
                if cursor1 is not None:
                    collection.update_one({"username": username},{"$set":{"phone": doc['phone']}})
                else:
                    collection.update_one({"username": username},{"$set":{"phone": phone}})
                    
            
                    
          
            collection.update_one({"username": username},{"$set":{"password": password}})
            collection.update_one({"username": username},{"$set":{"state": state}})
            collection.update_one({"username": username},{"$set":{"city": city}})
            collection.update_one({"username": username},{"$set":{"address": address}})
            collection.update_one({"username": username},{"$set":{"first_name": first_name}})
            collection.update_one({"username": username},{"$set":{"last_name": last_name}})
            
            #collection.update({"username": username, "password": password})
    return False


def getCustomerBusinessOrders(username):
    db = client.supply
    collection = db.client_restaurant_buyers_receipt
    
    cursor4 = collection.find_one({"customer_username": username})
    
    
    if cursor4 is None:

        return True
        
    

    clientItems = []

    for doc in collection.find({}):
        if doc['customer_username'] == username:
            thisdict = {
            "amount_of_sales" : doc['amount_of_sales'],
            "client_username" : doc['client_username'], 
            "customer_username" : doc['customer_username'], 
            "date" : doc['date'],
            "itemName" : doc['itemName'],
            "notes" : doc['notes'],
            "order_status" : doc['order_status'], 
            "price" : doc['price'], 
            "price_for_one" : doc['price_for_one'], 
            "restaurant_name" : doc['restaurant_name']
            }
            clientItems.append(thisdict)
            

    return clientItems

def getClientBusinessForCustomer(business_name):
    db = client.supply
    collection = db.client_register_restaurant_data
    collection2 = db.client_register_restaurant_data_items
    
    cursor4 = collection.find_one({"business_name": business_name})
    #cursor3 = collection2.find_one({"username": username})
    
    if cursor4 is None:

        return True
        
    #if cursor3 is None:

        #return True

    clientItems = []

    for doc in collection.find({}):
        if doc['business_name'] == business_name:
            dict = {
            'business_name': doc['business_name'],
            "username" : doc['username'], 
            "password" : doc['password'], 
            "business_motto" : doc['business_motto'],
            "business_description" : doc['business_description'],
            "business_email" : doc['business_email'],
            "business_phone" : doc['business_phone'], 
            "business_street" : doc['business_street'], 
            "business_city" : doc['business_city'], 
            "business_state" : doc['business_state'], 
            "sunday" : doc['sunday'], 
            "monday" : doc['monday'], 
            "tuesday" : doc['tuesday'], 
            "wednesday" : doc['wednesday'],
            "thursday" : doc['thursday'] , 
            "friday" : doc['friday'], 
            "saturday" : doc['saturday']

            }
            clientItems.append(dict)


    return clientItems

