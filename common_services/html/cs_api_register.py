import json
from demand_customer import Customer
import cs_database_utils


def cs_register(self, dictionary, db_utils):
    # Pull all the keys from the dictionary
    first_name = dictionary["first_name"]
    last_name = dictionary["last_name"]
    username = dictionary["username"]
    password = dictionary["password"]
    email = dictionary["email"]
    phone = dictionary["phone"]
    address = dictionary["address"]
    city = dictionary["city"]
    state = dictionary["state"]
    
    new_customer = Customer(first_name, last_name, username, password, address, city, state, phone, email)


    # insert dictionary into db
    result = cs_database_utils.register_user(new_customer)

    # if the insertion was successful return 200
    if result != None:
        return True
    # if user already exists return 403
    else:
        return False
